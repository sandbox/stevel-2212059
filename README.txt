
Menu Translation per Menu
https://drupal.org/sandbox/stevel/2212059

DESCRIPTION
-----------
By default, Drupal 7 allows only users with "Administer menus and menu items" to
translate menu items. In case you want for instance to let certain users
translate Main menu or Navigation menu but not Management menu, this module
provides this functionality. 

INSTALLATION
------------
Please read instructions at: https://drupal.org/sandbox/stevel/2212059

COMMON ISSUES
-------------
 * PROBLEM:
   I enabled the module, but my users still can't translate menu items. I have
   given them the "Administer /menu name/ menu items" permission:
   
   SOLUTION:
   Make sure the user also has the "Translate interface texts" permission.

CONTACT
-------
Use the contact form at https://drupal.org/user/569048/contact
