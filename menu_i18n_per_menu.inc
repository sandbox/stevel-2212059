<?php

/**
 * @file
 * File for the menu_i18n_per_menu__i18n_menu_link class.
 */

/**
 * This class extends the i18n_menu_link class to give access to specific menus.
 */
class menu_i18n_per_menu__i18n_menu_link extends i18n_menu_link {

  /**
   * Class constructor.
   */
  public function __construct($type, $key, $object) {
    parent::__construct($type, $key, $object);
  }

  /**
   * Access to object translation.
   *
   * This should check object properties and permissions.
   */
  protected function translate_access() {
    return _menu_admin_per_menu_menu_link_access($this->object) && user_access('translate interface');
  }

  /**
   * Access to object localization.
   *
   * This should check object properties and permissions.
   */
  protected function localize_access() {
    return _menu_admin_per_menu_menu_link_access($this->object) && user_access('translate interface');
  }

}
